﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NotesAppBlazor.Models
{
    public class Note
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow; 

        [Required]
        public string Content { get; set; }

        [NotMapped]
        public bool IsExpanded { get; set; }
    }
}
