﻿using NotesAppBlazor.Enums;

namespace NotesAppBlazor.Models
{
    public class BaseResponse<T>
    {
        public string Message { get; set; }
        public Status Status { get; set; }
        public T? Data { get; set; }
        public BaseResponse()
        {
            Status = Status.OK;
            Message = "Success: ";
        }

    }
}
