﻿using Microsoft.EntityFrameworkCore;
using NotesAppBlazor.Models;

namespace NotesAppBlazor.Data
{
    public class NotesContext : DbContext
    {
        public NotesContext(DbContextOptions<NotesContext> options) : base(options)
        { }

        public DbSet<Note> Notes { get; set; }
    }
}
