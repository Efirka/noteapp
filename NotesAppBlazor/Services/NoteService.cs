﻿using Microsoft.EntityFrameworkCore;
using NotesAppBlazor.Data;
using NotesAppBlazor.Enums;
using NotesAppBlazor.Models;
using System;

namespace NotesAppBlazor.Services
{
    public class NoteService
    {
        private readonly NotesContext _context;

        public NoteService(NotesContext context)
        {
            _context = context;
        }



        /// <summary>
        /// Отримати список нотаток відсортованих по даті
        /// </summary>
        /// <returns>Список нотаток або помилка</returns>
        public async Task<BaseResponse<List<Note>>> GetAllNotes()
        {
            var response = new BaseResponse<List<Note>>();
            try
            {
                response.Data = await _context.Notes.OrderByDescending(n => n.Created).ToListAsync();
            }
            catch (Exception ex)
            {
                response.Status = Status.Error;
                response.Message = $"Error: {ex.Message}";
            }
            return response;
        }

        /// <summary>
        /// Отримати нотатку за її ID.
        /// </summary>
        /// <param name="id">ID нотатки.</param>
        /// <returns>Нотатка або помилка </returns>
        public async Task<BaseResponse<Note>> GetNoteById(int id)
        {
            var response = new BaseResponse<Note>();
            try
            {
                var note = await _context.Notes.FindAsync(id);
                if (note == null)
                {
                    response.Status = Status.Error;
                    response.Message = "Note not found.";
                    return response;
                }
                response.Data = note;
            }
            catch (Exception ex)
            {
                response.Status = Status.Error;
                response.Message = $"Error: {ex.Message}";
            }
            return response;
        }



        /// <summary>
        /// Додати нову нотатку
        /// </summary>
        /// <param name="note">Нотатка для додавання.</param>
        /// <returns>Результат додавання (помилка або успішно) </returns>
        public async Task<BaseResponse<bool>> AddNote(Note note)
        {
            var response = new BaseResponse<bool>();
            try
            {
                _context.Notes.Add(note);
                await _context.SaveChangesAsync();
                response.Data = true;
            }
            catch (Exception ex)
            {
                response.Status = Status.Error;
                response.Message = $"Error: {ex.Message}";
                response.Data = false;
            }
            return response;
        }


        /// <summary>
        /// Оновити нотатку
        /// </summary>
        /// <param name="updatedNote">Нотатка з оновленими даними.</param>
        /// <returns>Результат оновлення (помилка або успішно) </returns>
        public async Task<BaseResponse<bool>> UpdateNote(Note updatedNote)
        {
            var response = new BaseResponse<bool>
            {
                Data = false,  
                Status = Status.Error,
                Message = "Note not found."
            };

            var existingNote = await _context.Notes.FindAsync(updatedNote.Id);
            if (existingNote != null)
            {
                try
                {
                    existingNote.Title = updatedNote.Title;
                    existingNote.Content = updatedNote.Content;
                    await _context.SaveChangesAsync();
                    response.Data = true;
                    response.Status = Status.OK;
                    response.Message = "Note successfully updated.";
                }
                catch (Exception ex)
                {
                    response.Message = $"Error: {ex.Message}";
                }
            }

            return response;
        }







        /// <summary>
        /// Видалити нотатку
        /// </summary>
        /// <param name="id">ID нотатки для видалення.</param>
        /// <returns>Результат видалення (помилка або успішно) </returns>
        public async Task<BaseResponse<bool>> DeleteNote(int id)
        {
            var response = new BaseResponse<bool>();
            try
            {
                var note = await GetNoteById(id);
                if (note.Data != null)
                {
                    _context.Notes.Remove(note.Data);
                    await _context.SaveChangesAsync(); 
                    response.Data = true;
                }
                else
                {
                    response.Status = Status.Error;
                    response.Message = "Note not found.";
                    response.Data = false;
                }
            }
            catch (Exception ex)
            {
                response.Status = Status.Error;
                response.Message = $"Error: {ex.Message}";
                response.Data = false;
            }
            return response;
        }

        /// <summary>
        /// Пошук нотаток за заголовком або вмістом.
        /// </summary>
        /// <param name="query">Рядок для пошуку.</param>
        /// <returns>Список нотаток, що відповідають критеріям пошуку та статус.</returns>
        public async Task<BaseResponse<List<Note>>> SearchNotes(string query)
        {
            BaseResponse<List<Note>> response = new BaseResponse<List<Note>>();

            try
            {
                if (string.IsNullOrWhiteSpace(query))
                {
                    return await GetAllNotes();
                }
                else
                {
                    query = query.ToLower();
                    response.Data = await _context.Notes
                        .Where(n => n.Title.Contains(query) || n.Content.Contains(query))
                        .OrderByDescending(n => n.Created)
                        .ToListAsync();
                    response.Message += "Retrieved filtered notes.";
                }
            }
            catch (Exception ex)
            {
                response.Status = Status.Error;
                response.Message = $"Error: {ex.Message}";
            }

            return response;
        }


    }
}
