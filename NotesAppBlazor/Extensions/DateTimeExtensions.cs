﻿using static System.Runtime.InteropServices.JavaScript.JSType;

namespace NotesAppBlazor.Extencive
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Форматує дату, повертаючи різницю між поточною датою та датою створення.
        /// </summary>
        /// <param name="date">Дата, яку потрібно форматувати.</param>
        /// <returns>Різниця у часі в форматі "Створено ** назад".</returns>
        public static string FormatDate(this DateTime date)
        {
            var timeSpan = DateTime.Now - date;

            if (timeSpan <= TimeSpan.FromMinutes(60))
                return $"Created {timeSpan.Minutes} minutes ago";
            if (timeSpan <= TimeSpan.FromHours(24))
                return $"Created {timeSpan.Hours} hours ago";
            if (timeSpan <= TimeSpan.FromDays(30))
                return $"Created {timeSpan.Days} days ago";

            return date.ToShortDateString();
        }
    }
}
