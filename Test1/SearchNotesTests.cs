﻿using Microsoft.EntityFrameworkCore;
using NotesAppBlazor.Data;
using NotesAppBlazor.Models;
using NotesAppBlazor.Services;

namespace UnitTest
{
    public class SearchNotesTests
    {
        [Fact]
        public async Task SearchNotes_WithValidQuery_ShouldReturnFilteredNotes()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase7")
                .Options;

            using (var context = new NotesContext(options))
            {
                var notesData = new List<Note>
        {
            new Note { Title = "Note 1", Content = "Content 1", Created = DateTime.Now },
            new Note { Title = "Note 2", Content = "Content 2", Created = DateTime.Now },
            new Note { Title = "Note 3", Content = "Content 3", Created = DateTime.Now },
            new Note { Title = "Note 4", Content = "Content 4", Created = DateTime.Now },
            new Note { Title = "Note 5", Content = "Content 5", Created = DateTime.Now },
            new Note { Title = "Note 6", Content = "Content 6", Created = DateTime.Now },
            new Note { Title = "Note 7", Content = "Content 7", Created = DateTime.Now }
        };
                context.Notes.AddRange(notesData);
                context.SaveChanges();
            }

            using (var context = new NotesContext(options))
            {
                var service = new NoteService(context);

                // Act
                var result = await service.SearchNotes("Note 1");

                // Assert
                Assert.NotNull(result.Data);
                Assert.Equal(1, result.Data.Count);
            }
        }



        [Fact]
        public async Task SearchNotes_WithEmptyQuery_ShouldReturnAllNotes()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase8")
                .Options;

            using (var context = new NotesContext(options))
            {
                var notesData = new List<Note>
        {
            new Note { Title = "Note 1", Content = "Content 1", Created = DateTime.Now },
            new Note { Title = "Note 2", Content = "Content 2", Created = DateTime.Now },
            new Note { Title = "Note 3", Content = "Content 3", Created = DateTime.Now },
            new Note { Title = "Note 4", Content = "Content 4", Created = DateTime.Now }
        };
                context.Notes.AddRange(notesData);
                context.SaveChanges();
            }

            using (var context = new NotesContext(options))
            {
                var service = new NoteService(context);

                // Act
                var result = await service.SearchNotes("");

                // Assert
                Assert.NotNull(result.Data);
                Assert.Equal(4, result.Data.Count);
            }
        }



        [Fact]
        public async Task SearchNotes_WithNonMatchingQuery_ShouldReturnEmptyList()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase9")
                .Options;

            using (var context = new NotesContext(options))
            {
                // Додати декілька нотаток до бази даних
                var notesData = new List<Note>
                {
                    new Note { Title = "Note 1", Content = "Content 1", Created = DateTime.Now },
                    new Note { Title = "Note 2", Content = "Content 2", Created = DateTime.Now }
                };
                context.Notes.AddRange(notesData);
                context.SaveChanges();

                // Створити сервіс, якому буде передано реальний контекст
                var service = new NoteService(context);

                // Act
                var result = await service.SearchNotes("NonMatchingQuery");

                // Assert
                Assert.NotNull(result.Data);
                Assert.Empty(result.Data); // Очікуємо, що не знайдено нотаток для неспівпадаючого запиту
            }
        }
    }
}
