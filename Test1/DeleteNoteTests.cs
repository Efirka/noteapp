﻿using Microsoft.EntityFrameworkCore;
using Moq;
using NotesAppBlazor.Data;
using NotesAppBlazor.Enums;
using NotesAppBlazor.Models;
using NotesAppBlazor.Services;
using System;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest
{
    public class DeleteNoteTests 
    {
        [Fact]
        public async Task DeleteNote_WithValidId_ShouldReturnTrue()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase2")
                .Options;

            using (var context = new NotesContext(options))
            {
                var initialNote = new Note { Title = "Initial Note", Content = "Initial Content", Created = DateTime.Now };
                context.Notes.Add(initialNote);
                context.SaveChanges();

                var service = new NoteService(context);

                // Act
                var result = await service.DeleteNote(initialNote.Id);

                // Assert
                Assert.True(result.Data);
            }
        }

        [Fact]
        public async Task DeleteNote_WithNonExistentId_ShouldReturnFalseAndErrorStatus()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase3")
                .Options;

            using (var context = new NotesContext(options))
            {
                var service = new NoteService(context);

                // Act
                var result = await service.DeleteNote(8); 

                // Assert
                Assert.False(result.Data);
                Assert.Equal(Status.Error, result.Status);
            }
        }
    }
}
