﻿using Microsoft.EntityFrameworkCore;
using NotesAppBlazor.Data;
using NotesAppBlazor.Enums;
using NotesAppBlazor.Models;
using NotesAppBlazor.Services;

namespace UnitTest
{
    public class GetNoteByIdTests
    {
        [Fact]
        public async Task GetNoteById_WithValidId_ShouldReturnNote()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase5")
                .Options;

            using (var context = new NotesContext(options))
            {
                context.Notes.Add(new Note { Id = 1, Title = "Note 1", Content = "Content 1", Created = DateTime.Now });
                context.SaveChanges();

                var service = new NoteService(context);

                // Act
                var result = await service.GetNoteById(1);

                // Assert
                Assert.NotNull(result.Data);
                Assert.Equal(1, result.Data.Id);
                Assert.Equal("Note 1", result.Data.Title);
            }
        }

        [Fact]
        public async Task GetNoteById_WithInvalidId_ShouldReturnErrorStatus()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase6")
                .Options;

            using (var context = new NotesContext(options))
            {

                var service = new NoteService(context);

                // Act
                var result = await service.GetNoteById(1);

                // Assert
                Assert.Null(result.Data);
                Assert.Equal(Status.Error, result.Status);
            }
        }


    }
}
