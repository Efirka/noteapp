﻿using Microsoft.EntityFrameworkCore;
using NotesAppBlazor.Data;
using NotesAppBlazor.Enums;
using NotesAppBlazor.Models;
using NotesAppBlazor.Services;

namespace UnitTest
{
    public class AddNoteTests
    {
        [Fact]
        public async Task AddNote_WithValidNote_ShouldReturnTrue()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase")
                .Options;

            using (var context = new NotesContext(options))
            {
                var service = new NoteService(context);

                var newNote = new Note { Title = "New Note", Content = "New Content", Created = DateTime.Now };

                // Act
                var result = await service.AddNote(newNote);

                // Assert
                Assert.True(result.Data);
            }
        }

        [Fact]
        public async Task AddNote_WithInvalidNote_ShouldReturnFalseAndErrorStatus()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase1")
                .Options;

            using (var context = new NotesContext(options))
            {
                var service = new NoteService(context);

                var invalidNote = new Note { Title = "Invalid Note" };

                // Act
                var result = await service.AddNote(invalidNote);

                // Assert
                Assert.False(result.Data);
                Assert.Equal(Status.Error, result.Status);
            }
        }
    }
}
