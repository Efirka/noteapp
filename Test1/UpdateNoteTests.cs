﻿using Microsoft.EntityFrameworkCore;
using NotesAppBlazor.Data;
using NotesAppBlazor.Enums;
using NotesAppBlazor.Models;
using NotesAppBlazor.Services;

namespace UnitTest
{
    public class UpdateNoteTests
    {
        [Fact]
        public async Task UpdateNote_WithValidNote_ShouldReturnTrue()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase10")
                .Options;

            using (var context = new NotesContext(options))
            {
                var initialNote = new Note { Title = "Initial Note", Content = "Initial Content", Created = DateTime.Now };
                context.Notes.Add(initialNote);
                context.SaveChanges();

                var service = new NoteService(context);

                var updatedNote = new Note { Id = initialNote.Id, Title = "Updated Note", Content = "Updated Content" };

                // Act
                var result = await service.UpdateNote(updatedNote);

                // Assert
                Assert.True(result.Data);
            }
        }

        [Fact]
        public async Task UpdateNote_WithNonExistentNote_ShouldReturnFalseAndErrorStatus()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase11")
                .Options;

            using (var context = new NotesContext(options))
            {
                var service = new NoteService(context);

                var updatedNote = new Note { Id = 1, Title = "Updated Note", Content = "Updated Content" };

                // Act
                var result = await service.UpdateNote(updatedNote);

                // Assert
                Assert.False(result.Data);
                Assert.Equal(Status.Error, result.Status);
            }
        }
    }
}
