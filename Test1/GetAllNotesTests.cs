using Microsoft.EntityFrameworkCore;
using NotesAppBlazor.Data;
using NotesAppBlazor.Models;
using NotesAppBlazor.Services;

namespace UnitTest
{
    public class GetAllNotesTests
    {
        [Fact]
        public async Task GetAllNotes_ShouldReturnListOfNotes()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<NotesContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase4")
                .Options;

            using (var context = new NotesContext(options))
            {
                context.Notes.AddRange(new List<Note>
                {
                    new Note { Id = 1, Title = "Note 1", Content = "Content 1", Created = DateTime.Now },
                    new Note { Id = 2, Title = "Note 2", Content = "Content 2", Created = DateTime.Now }
                });
                context.SaveChanges();

                var service = new NoteService(context);

                // Act
                var result = await service.GetAllNotes();

                // Assert
                Assert.NotNull(result.Data);
                Assert.Equal(2, result.Data.Count);
            }
        }
    }
}
